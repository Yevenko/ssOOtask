package utils

import (
	"unicode"
	"regexp"
	"strings"
)

const minPassLength = 8
const emailRx = `\w[\w\.-]+@\w[\w\.-]+\.[[:alpha:]]{2}`

var (
	hasDigit     func(string) bool
	hasUpLetter  func(string) bool
	hasLowLetter func(string) bool
	hasSpaces    func(string) bool
)

func init() {
	hasDigit = fabricaCondition(unicode.IsDigit)
	hasUpLetter = fabricaCondition(unicode.IsUpper)
	hasLowLetter = fabricaCondition(unicode.IsLower)
	hasSpaces = fabricaCondition(unicode.IsSpace)
}

func IsPasswordValid(pass string) bool {
	if len(pass) < minPassLength {
		return false
	}
	return hasDigit(pass) && hasUpLetter(pass) && hasLowLetter(pass) && !hasSpaces(pass)
}

func fabricaCondition(condition func(rune) bool) func(string) bool {
	return func(text string) bool {
		for _, char := range text {
			if condition(char) {
				return true
			}
		}
		return false
	}
}

func IsEmailValid(email string) bool {
	if !regexp.MustCompile(emailRx).MatchString(email) || hasSpaces(email) || strings.Count(email, "@") > 1 {
		return false
	}
	indexDot := strings.LastIndex(email, ".")
	if len(email[indexDot+1:]) > 3 {
		return false
	}
	return true
}

func IsNameValid(name string) bool {
	if len(name) == 0 {
		return false
	}
	for _, char := range name {
		if unicode.IsLetter(char) || unicode.IsDigit(char) {
			continue
		}
		return false
	}
	return true
}
