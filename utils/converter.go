package utils

import (
	"time"
	"strconv"
)

func StrToTime(days, hours, minutes string) time.Duration {
	var d, h, m int
	if x, err := strconv.Atoi(days); err == nil {
		d = x
	}
	if x, err := strconv.Atoi(hours); err == nil {
		h = x
	}
	if x, err := strconv.Atoi(minutes); err == nil {
		m = x
	}

	duration := time.Duration(d*24+h)*time.Hour + time.Duration(m)*time.Minute
	if duration < TimeExpiration {
		return TimeExpiration
	}
	return duration
}
