package utils

import (
	"fmt"
	"crypto/sha256"
	"crypto/rand"
	"net"
	"log"
)

func GenerateSha256(text string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(text)))
}

func GenerateId() string {
	buf := make([]byte, 32)
	rand.Read(buf)
	return fmt.Sprintf("%x", buf)
}

func GetIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

//func GetRemoteIP(request *http.Request)net.IP{
//	request.Header.Get("X-FORWARDED-FOR")
//	ip,port,err := net.SplitHostPort(request.RemoteAddr)
//}