package utils

import (
	"errors"
)

type WrongReceiver struct {}


type MessageIsRead struct {}

func (wr WrongReceiver) Error() error {
	return errors.New("wrong receiver")
}

func (wr MessageIsRead) Error() error {
	return errors.New("message is read")
}