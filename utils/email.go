package utils

import (
	"fmt"
	"os"
	"net/smtp"
)

var auth smtp.Auth

func init() {
	auth = smtp.PlainAuth("", HOST_EMAIL, HOST_PASS, HOST_SMPT)
}

func SendEmail(idMessage string, receiverEmail, senderEmail string) error {
	port := os.Getenv("PORT")
	host := os.Getenv("HOST")
	if len(host) == 0 {
		host = "localhost"
	}
	if len(port) == 0 {
		port = "1488"
	}
	link := fmt.Sprintf("http://%s:%s/readOTM/%s", host, port, idMessage)
	msg := fmt.Sprintf("To: %s\nSubject: One Time Message\n\nYou recive message from %s\n%s",
		receiverEmail, senderEmail, link)
	err := smtp.SendMail(HOST_SMPT+":587", auth, HOST_EMAIL, []string{receiverEmail}, []byte(msg))
	return err
}

