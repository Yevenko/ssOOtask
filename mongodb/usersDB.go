package mongodb

import (
	"softserve/oleksandrTasks/models"
	"gopkg.in/mgo.v2/bson"
	"errors"
	"softserve/oleksandrTasks/utils"
)

func (col OTMCollection) SignUp(name, email, password string) (string, error) {
	user, err := models.NewUser(name, email, password)
	if err != nil {
		return "", err
	}
	_, isPresent := col.isPresent(name, email)
	if isPresent {
		return "", errors.New("credentials are engaged")
	}
	col.collection.Insert(user.ToJson())
	return user.Id(), nil
}

func (col *OTMCollection) isPresent(userName, email string) (string, bool) {
	var user = new(models.JsonUser)
	col.collection.Find(bson.M{"name": userName}).One(&user)
	if user.Id != "" {
		return user.Id, true
	}
	col.collection.Find(bson.M{"email": email}).One(&user)
	if user.Id != "" {
		return user.Id, true
	}
	return "", false
}

func (col *OTMCollection) SignIn(nameOrEmail, password string) (string, error) {
	var name, email string
	if utils.IsNameValid(nameOrEmail) {
		name = nameOrEmail
	} else if utils.IsEmailValid(nameOrEmail) {
		email = nameOrEmail
	} else {
		return "", errors.New("wrong login credentials")
	}
	id, is := col.isPresent(name, email)
	if !is {
		return "", errors.New("wrong login or password")
	}
	jsonUser := col.GetUserById(id)
	if jsonUser.Password != utils.GenerateSha256(password) {
		return "", errors.New("wrong login or password")
	}
	return id, nil
}

func (col *OTMCollection) GetUserById(id string) *models.JsonUser {
	var user = new(models.JsonUser)
	col.collection.FindId(id).One(&user)
	return user
}

func (col *OTMCollection) GetEmailById(id string) string {
	return col.GetUserById(id).Email
}
