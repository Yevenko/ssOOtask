package mongodb

import (
	"os"
	"gopkg.in/mgo.v2"
)

var mongoUrl = os.Getenv("MONGO_URL")

type Session *mgo.Session

type OTMCollection struct {
	collection *mgo.Collection
}

func NewCollection(session *mgo.Session, dbName, collectionName string) *OTMCollection {
	return &OTMCollection{session.DB(dbName).C(collectionName)}
}

func NewSession() (Session, error) {
	session, err := mgo.Dial("localhost:27017")
	return session, err
}
