package mongodb

import (
	"softserve/oleksandrTasks/models"
	"time"
	"errors"
	"fmt"
)

func (col *OTMCollection) InsertMessageToDB(senderEmail, receiverEmail, text, pass string, expirationTime time.Duration) (string, error) {
	m, err := models.NewOneTimeMessage(senderEmail, receiverEmail, text, pass, expirationTime)
	if err != nil {
		return "", err
	}
	json := m.MessageToJson()
	err = col.collection.Insert(json)
	if err != nil {
		return "", err
	}
	return json.Id, nil
}

func (col *OTMCollection) ReadOTM(receiverEmail, messageId string) (*models.JsonOTM, error) {

	var msg = new(models.JsonOTM)

	col.collection.FindId(messageId).One(&msg)
	fmt.Println("^^^^^^^^^^^", msg)
	if receiverEmail != msg.To {
		//send notification to user
		return nil, errors.New("illegal access")
	}
	if int64(time.Now().Sub(msg.TimeCreation)) > msg.ExpirationTime {
		col.deleteOTM(messageId)
		return nil, errors.New("time is over")
	}
	col.deleteOTM(messageId)
	return msg, nil

}

func (col *OTMCollection) deleteOTM(messageId string) {
	col.collection.RemoveId(messageId)
}
