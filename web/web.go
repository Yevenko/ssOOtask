package web

import (
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"fmt"
	"net/http"
	"softserve/oleksandrTasks/mongodb"
	"os"
	"softserve/oleksandrTasks/models"
	"softserve/oleksandrTasks/utils"
)



var user *models.JsonUser
var userColl *mongodb.OTMCollection
var messageColl *mongodb.OTMCollection

type DataOut struct {
	Name string
	Err  error
	Otm models.JsonOTM
}

func RunWebService(port uint) {
	session, err := mongodb.NewSession()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	userColl = mongodb.NewCollection(session, "OOT", "Users")
	messageColl = mongodb.NewCollection(session, "OOT", "OTM")
	m := martini.Classic()
	staticOptions := martini.StaticOptions{Prefix: "web/assets"}
	m.Use(martini.Static("web/assets", staticOptions))

	m.Use(render.Renderer(render.Options{
		Directory:  "web/template",    // Specify what path to load the templates from.
		Layout:     "layout",          // Specify a layout template. Layouts can call {{ yield }} to render the current template.
		Extensions: []string{".html"}, // Specify extensions to load for templates.
		Charset:    "UTF-8",           // Sets encoding for json and html content-types. Default is "UTF-8".
		IndentJSON: true,              // Output human readable JSON
	}))

	m.Get("/", indexHandler)
	m.Get("/signup", signUpHandler)
	m.Get("/signin", signInHandler)
	m.Post("/addUser", addUserHandler)
	m.Post("/getUser", getUserHandler)
	m.Get("/logout", logOutHandler)
	m.Get("/otm", otmHandler)
	m.Get("/sendMessage", sendMessageHandler)
	m.Get("/readOTM/:id", readOTMHandler)
	m.Post("/sendMessage", sendMessage)
	m.RunOnAddr(fmt.Sprintf(":%d", port))

}

func indexHandler(rnd render.Render, request *http.Request) {
	var id, name string
	cookie, err := request.Cookie(utils.USER_COOKIE)
	fmt.Println("********* ", cookie, err)
	if err == nil {
		id = cookie.Value
		fmt.Println(id)
	}
	if id != "" {
		userL := userColl.GetUserById(id)
		name = userL.Name
	}
	rnd.HTML(200, "index", DataOut{Name: name})
}


