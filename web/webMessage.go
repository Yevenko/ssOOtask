package web

import (
	"github.com/martini-contrib/render"
	"net/http"
	"softserve/oleksandrTasks/utils"
	"fmt"
	"github.com/go-martini/martini"
)

func otmHandler(rnd render.Render, request *http.Request) {
	var id, name string
	cookie, err := request.Cookie(utils.USER_COOKIE)
	if err == nil {
		id = cookie.Value
	}
	if id != "" {
		userL := userColl.GetUserById(id)
		name = userL.Name
	} else {
		rnd.Redirect("/signup")
		return
	}
	rnd.HTML(200, "otm", DataOut{Name: name})
}

func sendMessageHandler(rnd render.Render) {
	rnd.HTML(200, "sendMessage", nil)
}

func sendMessage(rnd render.Render, request *http.Request) {
	cookie, _ := request.Cookie(utils.USER_COOKIE)
	senderEmail := userColl.GetEmailById(cookie.Value)
	idOtm, err := messageColl.InsertMessageToDB(
		senderEmail,
		request.FormValue("email"),
		request.FormValue("message"),
		"",
		utils.StrToTime(
			request.FormValue("days"),
			request.FormValue("hours"),
			request.FormValue("minutes")))
	if err != nil {
		rnd.HTML(400, "error", DataOut{Err: err})
		return
	}
	fmt.Println(idOtm)
	link := fmt.Sprintf("%d", idOtm)
	fmt.Println(link)
	err = utils.SendEmail(idOtm, request.FormValue("email"), senderEmail)
	if err != nil {
		rnd.HTML(400, "error", DataOut{Err: err})
		return
	}
	rnd.Redirect("/otm")
}

func readOTMHandler(rnd render.Render, request *http.Request, params martini.Params) {
	idOtm := params["id"]
	if idOtm == "" {
		rnd.Redirect("/")
		return
	}

	cookie, _ := request.Cookie(utils.USER_COOKIE)
	id := cookie.Value
	fmt.Println("id OTM: ",idOtm, "id user: ",id)
	email := userColl.GetEmailById(id)
	msg, err := messageColl.ReadOTM(email, idOtm)
	fmt.Println("#######",msg,err)
	if err != nil {
		rnd.HTML(400, "error", DataOut{Err: err})
		return
	}
	//if msg == nil {
	//	fmt.Println("alarmmmmmmmmmmmmmmm")
	//}
	rnd.HTML(200, "readOTM", DataOut{Otm: *msg})

}
