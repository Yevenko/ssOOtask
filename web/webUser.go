package web

import (
	"github.com/martini-contrib/render"
	"net/http"
	"fmt"
	"softserve/oleksandrTasks/utils"
)

func signUpHandler(rnd render.Render) {
	rnd.HTML(200, "signup", nil)
}

func getUserHandler(rnd render.Render, request *http.Request, writer http.ResponseWriter) {
	ne := request.FormValue("name")
	pass := request.FormValue("password")
	fmt.Println(ne, pass)
	id, err := userColl.SignIn(ne, pass)
	if err != nil {
		fmt.Println(err)
		rnd.HTML(400, "error", DataOut{Err: err})
		return
	}
	cookie := &http.Cookie{
		Name:  utils.USER_COOKIE,
		Value: id,
	}
	http.SetCookie(writer, cookie)
	rnd.Redirect("/")
}
func signInHandler(rnd render.Render, request *http.Request, writer http.ResponseWriter) {
	rnd.HTML(200, "signin", nil)
}

func addUserHandler(rnd render.Render, request *http.Request, writer http.ResponseWriter) {
	name := request.FormValue("name")
	email := request.FormValue("email")
	password := request.FormValue("password")
	id, err := userColl.SignUp(name, email, password)
	if err != nil {
		fmt.Println(err)
		rnd.HTML(400, "error", DataOut{Err: err})
		return
	}
	cookie := &http.Cookie{
		Name:  utils.USER_COOKIE,
		Value: id,
	}
	http.SetCookie(writer, cookie)
	rnd.Redirect("/")

}

func logOutHandler(rnd render.Render, writer http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:  utils.USER_COOKIE,
		Value: ""}
	http.SetCookie(writer, cookie)
	rnd.Redirect("/")
}
