package models

import (
	"time"
	"net"
	"softserve/oleksandrTasks/utils"
	"errors"
	"net/http"
)

type messager interface {
	ReadMessage(*user) (string, error)
}

type simpleMessage struct {
	id            string
	senderEmail   string
	receiverEmail string
	content       string
	timeCreation  time.Time
}

type oneTimeMessage struct {
	message        *simpleMessage
	isRead         bool
	pass           string
	expirationTime time.Duration
}

type notification struct {
	message *simpleMessage
	detalis readerDetails
}

type readerDetails struct {
	ip        net.IP
	userAgent string
	err       error
}

func (otm *oneTimeMessage) ReadMessage(reader *user) (string, error) {
	if otm.message.receiverEmail != reader.email {
		return "", utils.WrongReceiver{}.Error()
	}
	if !otm.isRead {
		return "", utils.MessageIsRead{}.Error()
	}
	otm.isRead = true
	return otm.message.content, nil
}

func NewOneTimeMessage(senderEmail, receiverEmail, text, pass string, expirationTime time.Duration) (*oneTimeMessage, error) {
	if len(text) == 0 || expirationTime < utils.TimeExpiration {
		return nil, errors.New("wrong message")
	}
	return &oneTimeMessage{
		message: &simpleMessage{
			id:            utils.GenerateId(),
			senderEmail:   senderEmail,
			receiverEmail: receiverEmail,
			content:       text,
			timeCreation:  time.Now()},
		isRead:         false,
		pass:           pass,
		expirationTime: expirationTime}, nil
}

func NewNotification(receiver *user, text, r *http.Request, err error) (messager, error) {
	return &notification{}, nil
}

func (n *notification) ReadMessage(u *user) (string, error) {
	return "", nil
}

func (otm *oneTimeMessage) MessageToJson() *JsonOTM {
	return &JsonOTM{
		Id:             otm.message.id,
		From:           otm.message.senderEmail,
		To:             otm.message.receiverEmail,
		Content:        otm.message.content,
		Password:       otm.pass,
		TimeCreation:   otm.message.timeCreation,
		ExpirationTime: int64(otm.expirationTime),
	}
}
