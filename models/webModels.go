package models

type RequestMessage struct {
	Email    string
	Message  string
	Password string
	Days     int
	Hours    int
	Minutes  int
}
