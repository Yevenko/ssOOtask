package models

import "time"

type JsonUser struct {
	Id       string `bson:"_id,omitempty"`
	Name     string
	Email    string
	Password string
}

type JsonOTM struct {
	Id             string `bson:"_id,omitempty"`
	From           string
	To             string
	Content        string
	TimeCreation   time.Time
	Password       string
	ExpirationTime int64
}

