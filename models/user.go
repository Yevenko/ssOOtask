package models

import (
	"softserve/oleksandrTasks/utils"
	"errors"
	"strings"
)

type user struct {
	id       string
	name     string
	email    string
	password string
}

func (u *user) Id() string {
	return u.id
}


func (u *user) setPassword(password string) error {
	if !utils.IsPasswordValid(password) {
		return errors.New("not valid password")
	}
	u.password = utils.GenerateSha256(password)
	return nil
}

func (u *user) setEmail(email string) error {
	if !utils.IsEmailValid(email) {
		return errors.New("not valid email")
	}
	u.email = email
	return nil
}

func (u *user) setName(name string) error {
	name = strings.TrimSpace(name)
	if !utils.IsNameValid(name) {
		return errors.New("not valid name")
	}
	u.name = name
	return nil
}

func NewUser(name, email, password string) (*user, error) {
	person := new(user)
	person.id = utils.GenerateId()
	if err := person.setName(name); err != nil {
		return nil, err
	}
	if err := person.setEmail(email); err != nil {
		return nil, err
	}
	if err := person.setPassword(password); err != nil {
		return nil, err
	}
	return person, nil
}

func (u *user) CheckPassword(password string) bool {
	return u.password == utils.GenerateSha256(password)
}

func (u *user) ToJson() *JsonUser {
	return &JsonUser{u.id, u.name, u.email, u.password}
}
