package main

import (
	"fmt"
	"softserve/oleksandrTasks/web"
	"os"
)

func main() {
	//u,err := models.NewUser("max","comdf@lmv.com","jvCjys7495")
	//fmt.Println(err,u)
	port := os.Getenv("PORT")


	host := os.Getenv("HOST")
	if len(host) == 0{
		host = "localhost"
	}
	if len(port) == 0 {
		port = "1488"
	}
	fmt.Printf("http://%s:%s\n", host, port)
	fmt.Println("http://127.0.0.1:1488")
	web.RunWebService(1488)

}
